**Model of the French marginal mix !**

A good knowledge of power system dynamics is necessary for the implementation of efficient demand-side management strategies. The best way to identify the impact of an interruption to local electricity demand is to evaluate the marginal mix of the power system. This article proposes a model of the French power system that can be used to assess the marginal mix for different demand-response durations. The model is based on two calibrated merit-orders to reproduce the daily and intra-daily dynamics, including the conservation of hydroelectric energy and electricity exchange, coupled with a simplified model of the European grid. The validation protocol focuses on the scope of the model (short-term interruption of load) and includes yearly and weekly timescales. The French marginal mix turns out to be seasonal and sensitive to the duration of the modification in consumption. Moreover, it is highly related to the mix of countries balancing the variations in  electricity exchanged between France and other countries.  


**Preview**

Figure 1:
![](image/Figure1.png)

**Dependencies**
*  Python 3

**Release history**
*  v0 : 20/01/2022


**License**

Copyright (C) ENGIE SA - ENGIE Lab CRIGEN - La Rochelle University/LaSIE - All Rights Reserved
* Proprietary and confidential
* Subject to French law on copyright
* Unauthorized copying of this file, via any medium is strictly prohibited: https://crigen.myengie.com/copyright
* Any request for authorization of use should be addressed by email to“mvalo.bvt@engie.com” with the subject “Copyright Statement in Source Code”


**References**

M. Biéron, J. Le Dréau, B. Haas. Development of a Unit Commitment model to assess the marginal technologies reacting to demand response events. Under review (2022)



